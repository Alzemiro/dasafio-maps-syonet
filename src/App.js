import React from 'react'
import Maps from './components/Maps'

function App() {
  return (
    <div className="container">   
      <Maps />
    </div> 
    )
}

export default App
