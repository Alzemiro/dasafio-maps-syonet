import React, { useState } from 'react'

import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Alert from '@material-ui/lab/Alert'
import { makeStyles } from '@material-ui/core/styles'

import {
  GoogleMap,
  useLoadScript,
  Marker,
  InfoWindow
} from '@react-google-maps/api'

//estilo inicial do mapa
const mapContainerStyle = {
  width: 'auto',
  height: '100vh'
}

//região inicial do maps, onde vai centralizar na tela
let center = {
  lat: -29.6838801,
  lng: -51.4601312
}

//estilo material UI
const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh'
  },
  paper: {
    margin: theme.spacing(8,
      4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

export default function Maps() {

  const [latitude, setLatitude] = useState('')
  const [longitude, setLongitude] = useState('')
  const [messages, setMessages] = useState('')
  const [markers, setMarkers] = useState([])


  const classes = useStyles()

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyCvSVTgL5ifCrbz7tLLvG__4x4hxNubEIU'
  })

  //Teste para saber se o mapa está carregando normalmente
  if (loadError) return 'Erro ao carregar o maps'
  if (!isLoaded) return 'Carregando maps'

  //Função para receber os novos marcadores, consiste em array de objetos com os dados necessários para 
  //criar o marcador e centralizar na tela com a latitude e longitude novas.
  //Junto virá a menssagem a ser exibida na <InfoWindow>
  function doSubmit(event) {
    event.preventDefault()    
      setMarkers((atual) => [
        ...atual, {
          lat: Number(latitude),
          lng: Number(longitude),
          message: messages,
          selected: true,
        }
      ])

      center = { lat: Number(latitude), lng: Number(longitude) }

      setLatitude('')
      setLongitude('')
      setMessages('')
  }

  //Duplicação de array necessária para exibir o InfoWindow
  const showMessage = (show, index) => {
    var temp = [...markers]
    markers[index].selected = show
    setMarkers(temp)
  }

  return (

    <Grid container component="main" className={classes.root}>

      <CssBaseline />

      <Grid item xs={12} sm={4} md={7}>

        <GoogleMap
          mapContainerStyle={mapContainerStyle}
          zoom={16}
          center={center}>


          {/* Percorre o Array de marcadores, buscando os dados de latitude e longitute para marcar no mapa */}
          {markers.map((marker, index) =>
            <>
              <Marker position={{
                lat: marker.lat,
                lng: marker.lng
              }}

                /* Armazena quando o marcador é clicado */
                onClick={() => showMessage(true, index)}>
                {/* Armazena o erro */}


              </Marker>

              {/* Verifica se o marcador já foi clicado e atribui null quando a InfoWindow é fechada */}
              {marker.selected ? <InfoWindow
                position={{
                  lat: marker.lat,
                  lng: marker.lng
                }}

                onCloseClick={() => showMessage(false, index)}>

                <div>
                  <h2>
                    {marker.message}
                  </h2>
                </div>
              </InfoWindow> : null}
            </>
          )}
        </GoogleMap>

      </Grid>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>

        <Grid className={classes.paper}>

          <form className={classes.form} noValidate onSubmit={doSubmit}>
            <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              value={latitude}
              onChange={event => setLatitude(event.target.value)}

              label="Latitude"
              name="latitude"
              autoFocus
            />
            <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              value={longitude}
              onChange={event => setLongitude(event.target.value)}
              name="longitude"
              label="Longitude"
              type="text"

            />
            <TextField
              variant="standard"
              margin="normal"
              required
              fullWidth
              value={messages}
              onChange={event => setMessages(event.target.value)}
              name="texto"
              label="Texto"
              type="text"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={!messages || !latitude || !longitude}
            >
              Marcar
            </Button>
          </form>
          
        </Grid>
      </Grid>
    </Grid>
  )
}
