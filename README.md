# Desafio Syonet - Maps React

Este desafio consiste na marcação e exibição de mensagem personalizada no google maps

## Instalação

Após clonar ou fazer do download deste diretorio navegue até a pasta desafio-maps-syonet

```bash
cd desafio-maps-syonet
```

Utilize o package manager [npm](https://www.npmjs.com/) para instalar os pacotes necessários para rodar a aplicação

```bash
npm install
```
## Inicialização

Após instalação dos pacotes basta rodar o comando

```bash
npm start
```

## Arquivo principal

A aplicação roda baseada em um componente principal ```Maps.js``` que se encontra dentro de ```dasafio-maps-syonet\src\components\Maps.js```

